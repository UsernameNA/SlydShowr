﻿/*Copyright (c) <2015> <Kyle Vogt>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/
window.onload = function () {
    var slideShowElements = document.getElementsByClassName("SlideShow");
    var slideShows = [];
    for (curElement = 0; curElement < slideShowElements.length; curElement++) {
        slideShows.push(new SlideShow(slideShowElements[curElement]));
    }
}

function SlideShow(element) {
    this.started = false;
    this.images = [];
    this.curImg = 0;
    this.element = element;
    this.rate = element.dataset.rate > 0 ? element.dataset.rate : 2000;
    try{
        this.srcList = element.getElementsByTagName("datalist")[0].children;
        for (var curSrc = 0; curSrc < this.srcList.length; curSrc++) {
            var img = new Image();
            img.src = this.srcList[curSrc].value;
            var self = this;
            img.onload = function (e) {
                self.imageLoaded(e);
            }
        }
    }catch(e){
        console.log(e.message);
    }
    
    this.imageLoaded = function (e) {
        if (typeof e.currentTarget == "object") {
            this.images.push(e.currentTarget)
            if (!this.started) {
                var self = this;
                this.started = true;
                self.update()
                setInterval(function () { self.update() }, this.rate);
            }
        }
    }
    this.update = function () {
        this.curImg++;
        if (this.curImg >= this.images.length) {
            this.curImg = 0;
        }
        this.element.innerHTML = ""; //clear out div
        this.images[this.curImg].width = this.element.clientWidth;
        this.images[this.curImg].height = this.element.clientHeight;
        this.element.appendChild(this.images[this.curImg]);
    }
}//SlideShow constructor